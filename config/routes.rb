Rails.application.routes.draw do
  get 'pages/index'
  resources :points, only: [:index] do
    collection do
      get :add_poi
      get :add_ror
      get :add_statement
      get :add_mod_speech
      get :add_mod_topic
      get :add_reso_speech
      get :add_amends
      get :add_amends_speech
      get :add_unmod
      get :add_activism
      get :add_lobby
      get :add_behaviour
      get :add_diplomacy
      get :add_fps
      get :add_fp_violation
      get :add_sponsor
    end
  end
  root to: 'pages#index'
end
