module ApplicationHelper
  def active_navigation(title, controller)
    'active' if title == controller            
  end
end
