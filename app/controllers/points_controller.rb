class PointsController < ApplicationController
  def add_poi
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)

      @delegate.poi_total = @delegate.poi_total + mark
      @delegate.poi_count = @delegate.poi_count + 1
      @delegate.poi_average = @delegate.poi_total / @delegate.poi_count
      @delegate.save
    end
  end

  def add_ror
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)

      @delegate.ror_total = @delegate.ror_total + mark
      @delegate.ror_count = @delegate.ror_count + 1
      @delegate.ror_average = @delegate.ror_total / @delegate.ror_count
      @delegate.save
    end
  end
  def add_statement
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)

      @delegate.statement_total = @delegate.statement_total + mark
      @delegate.statement_count = @delegate.statement_count + 1
      @delegate.statement_average = @delegate.statement_total / @delegate.statement_count
      @delegate.save
    end
  end
  def add_mod_speech
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)

      @delegate.mod_speech_total = @delegate.mod_speech_total + mark
      @delegate.mod_speech_count = @delegate.mod_speech_count + 1
      @delegate.mod_speech_average = @delegate.mod_speech_total / @delegate.mod_speech_count
      @delegate.save
    end
  end
  def add_mod_topic
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)

      @delegate.mod_topic_total = @delegate.mod_topic_total + mark
      @delegate.mod_topic_count = @delegate.mod_topic_count + 1
      @delegate.mod_topic_average = @delegate.mod_topic_total / @delegate.mod_topic_count
      @delegate.save
    end
  end
  def add_reso_speech
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)

      @delegate.reso_speech_total = @delegate.reso_speech_total + mark
      @delegate.reso_speech_count = @delegate.reso_speech_count + 1
      @delegate.reso_speech_average = @delegate.reso_speech_total / @delegate.reso_speech_count
      @delegate.save
    end
  end
  def add_amends
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)

      @delegate.amends_total = @delegate.amends_total + mark
      @delegate.amends_count = @delegate.amends_count + 1
      @delegate.amends_average = @delegate.amends_total / @delegate.amends_count
      @delegate.save
    end
  end
  def add_amends_speech
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)

      @delegate.amends_speech_total = @delegate.amends_speech_total + mark
      @delegate.amends_speech_count = @delegate.amends_speech_count + 1
      @delegate.amends_speech_average = @delegate.amends_speech_total / @delegate.amends_speech_count
      @delegate.save
    end
  end
  def add_unmod
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)
      @delegate.unmod = mark
      @delegate.save
    end
  end
  def add_activism
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)
      @delegate.activism = mark
      @delegate.save
    end
  end
  def add_lobby
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)
      @delegate.lobby = mark
      @delegate.save
    end
  end
  def add_behaviour
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)
      @delegate.behaviour = mark
      @delegate.save
    end
  end
  def add_diplomacy
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)
      @delegate.diplomacy = mark
      @delegate.save
    end
  end
  def add_fps
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)
      @delegate.fps = mark
      @delegate.save
    end
  end
  def add_fp_violation
    mark = params[:mark].to_i
    delegate_id = params[:delegate_id]
    unless mark < 0
      @delegate = Delegate.find(delegate_id)
      @delegate.fp_violation = mark
      @delegate.save
    end
  end
  def add_sponsor
    mark = params[:mark]
    delegate_id = params[:delegate_id]

      @delegate = Delegate.find(delegate_id)
      @delegate.sponsor = mark
      @delegate.save

  end
end
