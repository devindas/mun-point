# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150813210329) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "delegates", force: :cascade do |t|
    t.string   "country"
    t.integer  "speaker_list_speech",   default: 0
    t.integer  "poi_total",             default: 0
    t.integer  "poi_count",             default: 0
    t.integer  "poi_average",           default: 0
    t.integer  "ror_total",             default: 0
    t.integer  "ror_count",             default: 0
    t.integer  "ror_average",           default: 0
    t.integer  "statement_total",       default: 0
    t.integer  "statement_count",       default: 0
    t.integer  "statement_average",     default: 0
    t.integer  "mod_speech_total",      default: 0
    t.integer  "mod_speech_count",      default: 0
    t.integer  "mod_speech_average",    default: 0
    t.integer  "mod_topic_total",       default: 0
    t.integer  "mod_topic_count",       default: 0
    t.integer  "mod_topic_average",     default: 0
    t.integer  "reso_speech_total",     default: 0
    t.integer  "reso_speech_count",     default: 0
    t.integer  "reso_speech_average",   default: 0
    t.integer  "amends_total",          default: 0
    t.integer  "amends_count",          default: 0
    t.integer  "amends_average",        default: 0
    t.integer  "amends_speech_total",   default: 0
    t.integer  "amends_speech_count",   default: 0
    t.integer  "amends_speech_average", default: 0
    t.integer  "unmod",                 default: 0
    t.boolean  "sponsor",               default: false
    t.integer  "activism",              default: 0
    t.integer  "lobby",                 default: 0
    t.integer  "behaviour",             default: 0
    t.integer  "diplomacy",             default: 0
    t.integer  "fps",                   default: 0
    t.integer  "fp_violation",          default: 0
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "delegates", ["country"], name: "index_delegates_on_country", unique: true, using: :btree

end
