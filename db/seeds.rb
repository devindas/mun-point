# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

countries = %w(Japan Thailand Canada Serbia Vietnam Cambodia Malaysia Italy Dominican-Republic United-Republic-of-Tanzania Republic-of-Korea Spain Congo Tajkistan South-Africa Tunisia Maldives Ecuador Singapore Ethiopia Trinidad-and-Tobago Guinea Czech-Republic Honduras Somalia Gabon Lebanon Slovenia Pakistan Germany Panama Ghana Belize China Mynmar Georgia Portugal Nigeria Rwanda Philippines Romania Djibouti Algeria Zimbabwe Bhutan South-Sudan Kuwait Lao-Peoples-Democratic-Republic Qatar Poland Uruguay Bulgaria Nicaragua Cuba Hungary Saudi-Arabia Peru Mexico Finland United-States-of-America Bangladesh Iceland Burkina-Faso Australia Angola Nepal Islamic-Republic-of-Iran Russian-Federation Sudan Ireland Malta Switzerland Iraq Bharain Syrian-Arab-Republic Democratic-Republic-of-the-Congo Norway Guatemala Eritrea Morocco Chile Brunei-Darussalam Brazil India Kyrgyzstan Egypt Colombia Mongolia Netherlands Turkey Mauritius Kenya United-Kingdom-of-Great-Britain-and-Northern-Ireland Denmark Democratic-People's-Republic-of-Korea Austria Belarus Ukraine Argentina Indonesia Chad Zambia Guyana Israel Croatia)

countries.each{ |country|
  Delegate.create(country: country)
}
