class CreateDelegates < ActiveRecord::Migration
  def change
    create_table :delegates do |t|
      t.string  :country
      t.integer :speaker_list_speech, default: 0

      t.integer :poi_total, default: 0
      t.integer :poi_count, default: 0
      t.integer :poi_average, default: 0

      t.integer :ror_total, default: 0
      t.integer :ror_count, default: 0
      t.integer :ror_average, default: 0

      t.integer :statement_total, default: 0
      t.integer :statement_count, default: 0
      t.integer :statement_average, default: 0

      t.integer :mod_speech_total, default: 0
      t.integer :mod_speech_count, default: 0
      t.integer :mod_speech_average, default: 0

      t.integer :mod_topic_total, default: 0
      t.integer :mod_topic_count, default: 0
      t.integer :mod_topic_average, default: 0

      t.integer :reso_speech_total, default: 0
      t.integer :reso_speech_count, default: 0
      t.integer :reso_speech_average, default: 0

      t.integer :amends_total, default: 0
      t.integer :amends_count, default: 0
      t.integer :amends_average, default: 0

      t.integer :amends_speech_total, default: 0
      t.integer :amends_speech_count, default: 0
      t.integer :amends_speech_average, default: 0

      t.integer :unmod, default: 0
      t.boolean :sponsor, default: false
      t.integer :activism, default: 0
      t.integer :lobby, default: 0
      t.integer :behaviour, default: 0
      t.integer :diplomacy, default: 0
      t.integer :fps, default: 0
      t.integer :fp_violation, default: 0

      t.timestamps null: false
    end
  end
end
