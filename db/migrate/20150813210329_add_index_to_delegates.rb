class AddIndexToDelegates < ActiveRecord::Migration
  def change
    add_index :delegates, :country, unique: true
  end
end
